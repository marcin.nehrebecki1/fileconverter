<?php

namespace App\Command;

use App\Exception\DontHaveImageException;
use App\Service\LoadImageService;
use App\Service\SaveToCloudInterface;
use App\Service\ScaleImageService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class MiniatureImageCommand extends Command
{
    protected static $defaultName = 'miniature:image';
    protected static $defaultDescription = 'Add a short description for your command';
    private ScaleImageService $scaleImageService;
    private ParameterBagInterface $params;
    private SaveToCloudInterface $cloudService;
    private LoadImageService $loadImageService;

    public function __construct(
        ScaleImageService $scaleImageService,
        SaveToCloudInterface $cloudService,
        LoadImageService $loadImageService,
        ParameterBagInterface $params
    )
    {
        parent::__construct();
        $this->scaleImageService = $scaleImageService;
        $this->loadImageService = $loadImageService;
        $this->cloudService = $cloudService;
        $this->params = $params;
    }


    protected function execute(
        InputInterface $input,
        OutputInterface $output
    ): int
    {

        try {
            $imageDto = $this->loadImageService->loadAllImage();
            $this->scaleImageService->scaleFile($imageDto);
            $this->cloudService->saveAllImage($this->loadImageService->loadAllImage(true));
        } catch (DontHaveImageException $exception) {
            return Command::INVALID;
        } catch (\Exception $exception) {
            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }
}
