<?php

namespace App\Service;

use App\Dto\ImagesDTO;
use App\Exception\DontHaveImageException;

class LoadImageService extends AbstractService
{
    /**
     * @return ImagesDTO
     * @throws DontHaveImageException
     */
    public function loadAllImage(bool $loadSmallImage = false): ImagesDTO
    {
        $url = $this->baseDir . ($loadSmallImage ? $this->dirImageConvert : $this->dirImageToConvert);

        $images = glob($url."*.*");
        $imagesDto = new ImagesDTO();
        foreach($images as $image) {
            if($this->isImage($image)) {
                $imagesDto->images[] = $image;
            }
        }

        if (empty($imagesDto->images)) {
            throw new DontHaveImageException();
        }

        return $imagesDto;
    }

    private function isImage($image){
        $extension = strtolower(pathinfo($image, PATHINFO_EXTENSION));
        $imgExtArr = ['jpeg', 'png'];
        if(in_array($extension, $imgExtArr)){
            return true;
        }
        return false;
    }
}