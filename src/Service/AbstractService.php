<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

abstract class AbstractService
{

    protected int $maxImageSide;
    protected string $dirImageConvert;
    protected string $baseDir;
    protected string $dirImageToConvert;
    protected string $hostname;

    public function __construct(ParameterBagInterface $params)
    {
        $this->maxImageSide = $params->get('max_image_size');
        $this->hostname = $params->get('host_name');
        $this->dirImageConvert =  $params->get('dir.image_convert');
        $this->dirImageToConvert =  $params->get('dir.image_to_convert');
        $this->baseDir = $params->get('kernel.project_dir');
        if (!is_dir($this->baseDir . $this->dirImageConvert)) {
            mkdir($this->baseDir . $this->dirImageConvert);
        }
    }
}