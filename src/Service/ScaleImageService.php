<?php

namespace App\Service;

use App\Dto\ImagesDTO;
use App\Exception\ImageSizeException;

class ScaleImageService extends AbstractService
{
    public function scaleFile(ImagesDTO $imagesDTO)
    {
        foreach ($imagesDTO->images as $imageFilename) {
            try {
                [$width, $high] = $this->getNewImageSize($imageFilename);
                $arrayImageFileName = \explode('/', $imageFilename);
                $imageName = \end($arrayImageFileName);

                $gdImage = $this->getGdImage($imageFilename);
                $newImage =  \imagescale($gdImage, $width, $high);

                \imagepng($newImage, $this->baseDir . $this->dirImageConvert.$imageName);
            } catch (\Exception $exception) {
                continue;
            }
        }
    }

    public function getImageSize(string $fileName): ?array
    {
        $imageSize = getimagesize($fileName);
        if(!$imageSize) {
            throw new ImageSizeException();
        }

        return [$imageSize[0], $imageSize[1]];
    }

    private function getGdImage(string $fileName)
    {
        $ext = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));

        switch ($ext) {
            case 'png':
                return imagecreatefrompng($fileName);
            case 'jpeg':
                return imagecreatefromjpeg($fileName);
        }
    }

    private function getNewImageSize(mixed $imageFilename)
    {
        [$width, $high] = $this->getImageSize($imageFilename);

        if ($width >= $high) {
            $scale = round($width/$this->maxImageSide);
            $width = $this->maxImageSide;
            $high = round($high / $scale);
        } else {
            $scale = round($high/$this->maxImageSide);
            $high = $this->maxImageSide;
            $width = round($width / $scale);
        }

        return [$width, $high];
    }
}