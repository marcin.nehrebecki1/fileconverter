<?php

namespace App\Service;

use App\Dto\ImagesDTO;

class SaveToCloud extends AbstractService implements SaveToCloudInterface
{

    public function saveAllImage(ImagesDTO $imagesDTO): void
    {
        foreach ($imagesDTO->images as $fileName) {
            $pathInfo = pathinfo($fileName);
            $options = [ "gs" => [ "Content-Type" => "image/".$pathInfo['extension']]];
            $ctx = stream_context_create($options);
            $imageHostName = $this->hostname.$pathInfo['basename'];
            file_put_contents($imageHostName, $fileName, 0, $ctx);
        }
    }
}