<?php

namespace App\Service;

use App\Dto\ImagesDTO;

interface SaveToCloudInterface
{
    public function saveAllImage(ImagesDTO $imagesDTO): void;
}