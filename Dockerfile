FROM php:8.1.1-fpm
RUN apt-get clean && apt-get update \
    &&  apt-get install -y --no-install-recommends \
        locales \
        apt-utils \
        git \
        libicu-dev \
        g++ \
        libpng-dev \
        libxml2-dev \
        libzip-dev \
        libonig-dev \
        libxslt-dev \
        unzip \
        libpq-dev \
        nodejs \
        npm \
        wget \
        apt-transport-https \
        lsb-release \
        ca-certificates \
        libfreetype-dev \
        libjpeg-dev \
        optipng \
        jpegoptim \
        pngquant \
        gifsicle
RUN echo "en_US.UTF-8 UTF-8" > /etc/locale.gen  \
    &&  locale-gen
RUN curl -sS https://getcomposer.org/installer | php -- \
    &&  mv composer.phar /usr/local/bin/composer
RUN curl -sS https://get.symfony.com/cli/installer | bash \
    &&  mv /root/.symfony5/bin/symfony /usr/local/bin
RUN docker-php-ext-configure \
            intl \
    &&  docker-php-ext-install \
      pdo pdo_mysql pdo_pgsql opcache intl zip calendar dom mbstring xsl soap
RUN docker-php-ext-install bcmath
RUN docker-php-ext-configure gd --with-freetype --with-jpeg
RUN docker-php-ext-install gd
RUN pecl install apcu && docker-php-ext-enable apcu
RUN npm install --global yarn
WORKDIR /var/www/
RUN apt-get install -y libmagickwand-dev
RUN apt-get install -y imagemagick
RUN pecl install imagick
RUN docker-php-ext-enable imagick
RUN pecl install xdebug && docker-php-ext-enable xdebug
#FOR DEBBUGER
RUN echo 'xdebug.mode=develop,debug' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.start_with_request=yes' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.client_host=host.docker.internal' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.discover_client_host=1' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.idekey=PHPSTROM' >> /usr/local/etc/php/php.ini
RUN echo 'session.save_path = "/tmp"' >> /usr/local/etc/php/php.ini
RUN echo 'xdebug.output_dir="/tmp/profiler"' >> /usr/local/etc/php/php.ini