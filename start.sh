#!/bin/bash
export $(grep -v '#.*' docker.env | xargs)
docker-compose -f docker-compose.yml down
docker-compose -f docker-compose.yml build
docker-compose -f docker-compose.yml up -d
docker-compose -f docker-compose.yml exec -T app composer install
