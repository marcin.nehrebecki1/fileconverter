# Worker



## Getting started

If you like to start project to run script 
`./start.sh`
 
## Require

- docker


## Configuration

In env file may be change:
```
DIR_IMAGE_TO_CONVERT=/public/ImageToConvert/
DIR_IMAGE_CONVERTED=/public/ImageConvert/
MAX_IMAGE_SIZE=150
HOST_NAME=/public
```

##Run script
In docker you run 
- php bin/console miniature:image